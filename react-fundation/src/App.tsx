
import './App.css'
import  Counter  from './components/Counter';

import BasicTypes  from './typescript/BasicTypes';
import ObjectLiterals from './typescript/ObjectLiterals';
import BasicFunctions from './typescript/BasicFunctions';



function App() {

  return (
    <main>
      {/* <h1>Introduccion TS react</h1> */}
      {}
      {/* <BasicTypes />
      <ObjectLiterals /> */}
      {/* <BasicFunctions /> */}
      {<Counter/>}

      
    </main>
  )
}

export default App
