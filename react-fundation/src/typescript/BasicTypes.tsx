import React from 'react'

const BasicTypes = () => {
    const name: String = 'Javier';
    const age: number = 41;
    const isActive: boolean = true;
    const powers: string[] = ['React', 'ReacNative', 'otros'];


    return (
    <>
      <h1>Tipos Basicos</h1>
      { name } { age } { isActive ? 'true':'false' } 
      <br></br>
      { powers.join(', ')}
    </>
  )
}

export default BasicTypes

